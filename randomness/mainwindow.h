#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Network.h"
#include <thread>
#include <map>

class GameWindow;
class MessageHandler;
class QListWidgetItem;

namespace Ui {
class MainWindow;
}

struct TextMessage
{
    TextMessage(std::string text, bool priv, std::string color)
    {
        this->text = text;
        this->priv = priv;
        this->color = color;
    }

    ~TextMessage()
    {
        text.clear();
        color.clear();
    }

    std::string text;
    bool priv;
    std::string color;
};

typedef std::map<uint64 /*time*/, TextMessage* /*text*/> MessageMap;

struct Host
{
    MessageMap messages;
    std::string name;
};

extern std::map<SOCKET, Host> names;
extern SOCKET server;
extern SOCKET socketfd;

class MainWindow : public QMainWindow
{
    friend class TextEdit;
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    GameWindow* GetGame(uint16 id);
    void SetGame(GameWindow* game);
    void StopGame(uint16 id, bool freePtr = false);
private slots:
    void on_messageBrowser_anchorClicked(const QUrl &arg1);

    void on_clientsView_itemDoubleClicked(QListWidgetItem *item);

    void on_RollButton_pressed();

    void on_clientsView_itemPressed(QListWidgetItem *item);

    void on_RegisterButton_clicked();

    void HandleLabyrinth();

private:
    void HandleRightClickClientWhisper();
    void HandleRightClickClientTicTacToe();
    void HandleRightClickLabyrinth();
    Ui::MainWindow *ui;
    void SetDisconnected();
    void ConnectToServer();
    std::string format(int val);
    void LoadConfigLine(std::string line);
    std::string GetTimeOfTheDay(uint64 timestamp);
    void RecvMsg();
    void HandleColorSettings();
    void HandleIPSettings();
    void HandleNameSettings();
    void HandleGameTicTacToe();
    void HandleTrollGame();
    std::thread* netThrd;
    MessageHandler* msgHandler;
    std::map<uint16 /*id*/, GameWindow* /*game*/> games;
    bool stopped;
};

#endif // MAINWINDOW_H
