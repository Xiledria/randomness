#ifndef TROLLGAME_H
#define TROLLGAME_H

#include "GameWindow.h"
#include "ui_TrollGame.h"

class TrollGame : public GameWindow
{
    Q_OBJECT

public:
    explicit TrollGame(MainWindow* parent = 0);

protected:
    void HandleMessage(Message* msg) override;
    void SetData(uint16 index, int32 value) override;
    void changeEvent(QEvent *e);

private:
    Ui::TrollGame ui;
};

#endif // TROLLGAME_H
