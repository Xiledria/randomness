#include "GameWindow.h"
#include <QEvent>

GameWindow::GameWindow(MainWindow *parent) : QMainWindow(nullptr)
{
    mainWindow = parent;
}

GameType GameWindow::GetGameType()
{
    return gameType;
}

GameWindow::~GameWindow()
{
    mainWindow->StopGame(gameType, true);
}

void GameWindow::changeEvent(QEvent* e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
        case QEvent::ActivationChange:
            if (isHidden())
                mainWindow->StopGame(gameType);
            break;
        default:
            break;
    }
}
