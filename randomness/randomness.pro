#-------------------------------------------------
#
# Project created by QtCreator 2014-11-23T00:01:14
#
#-------------------------------------------------
QMAKE_CXXFLAGS += -std=c++14

QT       += core gui

QT += widgets

win32:QMAKE_CXXFLAGS +=  -static-libgcc -static-libstdc++ -L -pthread

linux:CONFIG += qt static

TEMPLATE = app
win32:CONFIG += static
TARGET = randomness
TEMPLATE = app

INCLUDEPATH += \
               ../custom/utilities

SOURCES += main.cpp\
    mainwindow.cpp \
    textedit.cpp \
    ../custom/utilities/Message.cpp \
    ../custom/utilities/Network.cpp \
    ../custom/utilities/Functions.cpp \
    config.cpp \
    MessageHandler.cpp \
    Piskvorky.cpp \
    GameWindow.cpp \
    Labyrinth.cpp \
    TrollGame.cpp

HEADERS  += mainwindow.h \
    textedit.h \
    ../custom/utilities/Message.h \
    ../custom/utilities/Network.h \
    ../custom/utilities/SharedDefs.h \
    ../custom/utilities/Functions.h \
    ../custom/utilities/cssrt.h \
    ../custom/utilities/Timer.h \
    config.h \
    MessageHandler.h \
    Piskvorky.h \
    GameWindow.h \
    Labyrinth.h \
    TrollGame.h

FORMS    += mainwindow.ui \
    Piskvorky.ui \
    TrollGame.ui \
    Labyrinth.ui

OTHER_FILES +=
