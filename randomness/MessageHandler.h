#ifndef MESSAGEHANDLER_H
#define MESSAGEHANDLER_H

#include "SharedDefs.h"
#include "Network.h"
#include "Message.h"

namespace Ui
{
class MainWindow;
}

typedef std::function<void(Message*)> MessageHandlerFnPtr;

class MessageHandler
{
public:
    MessageHandler(Ui::MainWindow* ui);

    MessageHandlerFnPtr operator[](MessageType t)
    {
        return handler[int(t)];
    }

    void HandleInvalid(Message* msg);
    void HandleAuth(Message* msg);
    void HandleMsgBroadcast(Message* msg);
    void HandleMsgWhisper(Message* msg);
    void HandleSetNickname(Message* msg);
    void HandleNameRefused(Message* msg);
    void HandleClientDisconnected(Message* msg);
    void HandleRoll(Message* msg);
    void HandleTicTacToeChallenge(Message* msg);
    void HandleTicTacToeStartGame(Message* msg);
    void HandleLabyrinthChallenge(Message* msg);
    void HandleLabyrinthStartGame(Message* msg);
    void ShowError(const char* msg);
    void ShowInfo(const char* msg);
private:
    void ShowPrivateMessage(const char* msg, SOCKET id, uint64 time, bool sender, const char* color = nullptr);
    void ShowGlobalMessage(const char* msg, SOCKET id, uint64 time, const char* color = nullptr);
    void ShowMessage(const char* msg, SOCKET id, uint64 time, const char* color = nullptr, const char* bgcolor = "white", const char* pretext = "");
    std::string format(int val);
    std::string GetTimeOfTheDay(uint64 timestamp);
    Ui::MainWindow* ui;
    MessageHandlerFnPtr handler[int(MessageType::MESSAGES_END)];
};

#endif // MESSAGEHANDLER_H
