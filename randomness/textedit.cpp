#include "textedit.h"
#include "QKeyEvent"
#include "ui_mainwindow.h"
#include "mainwindow.h"
#include "Network.h"
#include <QScrollBar>
#include "config.h"
#include <iostream>

textedit::textedit(QWidget *parent) :
    QTextEdit(parent)
{

}

void textedit::keyPressEvent(QKeyEvent *e)
{
    switch(e->key())
    {
        case Qt::Key_Return: //enter
        case Qt::Key_Enter:  //enter (num)
        {
            if ((e->modifiers() & 0x1E000000) != 0)
            {
                e->setModifiers(Qt::KeyboardModifier::NoModifier);
                return QTextEdit::keyPressEvent(e);
            }
            QString str = "";
            str += toPlainText();
            bool ret = true;
            if (!str.isEmpty())
            {
                for (auto i : str)
                {
                    if (!i.isSpace())
                    {
                        ret = false;
                        break;
                    }
                }
            }

            if (ret)
            {
                this->setText("");
                return;
            }
            MessageType type = MessageType::MSG_BROADCAST;
            SOCKET receiver = 0;
            if (str[0] == QString("/")[0] && str.contains("/w"))
            {
                QString nick = str.split(" ").at(1);
                type = MessageType::MSG_WHISPER;
                std::string stdname  = nick.toStdString();
                for (auto itr : names)
                {
                    if (itr.second.name != stdname)
                        continue;

                    receiver = itr.first;
                    break;
                }
                // slash w space & space after nick = 4
                str.remove(0, 4 + nick.size());
            }
            str+= "\n";
            uint8 fontsize = config->fontcolor.size();
            uint16 size = str.toStdString().size() + fontsize + sizeof(fontsize);
            if (type == MessageType::MSG_WHISPER)
            {
                if (!receiver)
                    break;

                size += sizeof(receiver);
            }

            Message* msg = new Message(type, size);
            if (type == MessageType::MSG_WHISPER)
                *msg << receiver;
            *msg << fontsize;
            if (fontsize)
                msg->Append(config->fontcolor.c_str());
            *msg << str.toStdString();
            msg->Send(socketfd);
            str.clear();
            //no break intended
        }
        case Qt::Key_Escape: //escape
            setText("");
            break;
        default: // default qt handler for key press
            QTextEdit::keyPressEvent(e);
    }
}
