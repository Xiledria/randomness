#include "Labyrinth.h"
#include <QPushButton>
#include <iostream>
#include <QKeyEvent>
#include <sstream>
#include <iomanip>
#include <QDesktopWidget>
#include <QPalette>

using namespace std::chrono_literals;

Labyrinth::Labyrinth(MainWindow* parent) :
    GameWindow(parent)
{
    gameType = GameType::LABYRINTH;
    opponent = nullptr;
    QDesktopWidget desktop;
    uint16 desktopHeight = desktop.geometry().height();
    uint16 desktopWidth = desktop.geometry().width();

    ui.setupUi(this);
    ui.sliderX->setMaximum(desktopWidth / 20 - 1);
    desktopWidth = std::min(desktopWidth, (uint16)800);
    ui.sliderX->setValue(desktopWidth / 20 - 1);
    ui.sliderX->setMinimum(10);
    ui.sliderY->setMaximum(desktopHeight / 20 - 4);
    desktopHeight = std::min(desktopHeight, (uint16)640);
    ui.sliderY->setValue(desktopHeight / 20 - 4);
    ui.sliderY->setMinimum(10);
    ui.generateButton->setFocusPolicy(Qt::FocusPolicy::NoFocus);
    ui.resetButton->setFocusPolicy(Qt::FocusPolicy::NoFocus);
    ui.sliderX->setFocusPolicy(Qt::FocusPolicy::NoFocus);
    ui.sliderY->setFocusPolicy(Qt::FocusPolicy::NoFocus);
    ui.hideButton->setFocusPolicy(Qt::FocusPolicy::NoFocus);
    buttons = nullptr;
    hidden = false;
    timerActive = false;
    timerThread = nullptr;
    arrow = 0;
    // GenerateNewLabyrinth();
}

Labyrinth::~Labyrinth()
{
    timerActive = false;
    if (timerThread)
        timerThread->join(); // wait for thread to stop

    if (buttons)
        ClearButtons();
}

void Labyrinth::HandleMessage(Message* msg)
{
    switch(msg->GetId())
    {
        case MessageType::LABYRINTH_GAME_SEND_ARRAY:
        {
            SOCKET a;
            *msg >> a;
            *msg >> xButtons;
            *msg >> yButtons;
            *msg >> hidden;
            if (buttons)
                QMetaObject::invokeMethod(this, "ClearButtons");
            QMetaObject::invokeMethod(this, "HandleReceivedArray", Q_ARG(QString, msg->GetString()));
            break;
        }
        case MessageType::LABYRINTH_GAME_SEND_MOVE:
        {
            SOCKET a;
            *msg >> a;

            // position we are leaving
            if (opponent->x == pos.x && opponent->y == pos.y)
                buttons[opponent->x][opponent->y]->setText(PLAYER_POSITION);
            else
                buttons[opponent->x][opponent->y]->setText(EMPTY_SPACE);

            *msg >> opponent->x;
            *msg >> opponent->y;

            // now check if player is not on position where opponent is going
            if (opponent->x == pos.x && opponent->y == pos.y)
                buttons[opponent->x][opponent->y]->setText(OPPONENT_AND_PLAYER_POSITION);
            else
                buttons[opponent->x][opponent->y]->setText(OPPONENT_POSITION);
        }
        default:
            break;
    }
}

void Labyrinth::ClearButtons()
{
     for (uint8 x = 0; x < oldXButtons; ++x)
     {
         for (uint8 y = 0; y < oldYButtons; ++y)
             buttons[x][y]->deleteLater();
         delete[] buttons[x];
     }
     delete[] buttons;
     buttons = nullptr;
}

void Labyrinth::HandleReceivedArray(QString array)
{
    buttons = new LabyrinthButton**[xButtons];
    for (uint8 x = 0; x < xButtons; ++x)
        buttons[x] = new LabyrinthButton*[yButtons];

    for (uint8 x = 0; x < xButtons; ++x)
    {
        for (uint8 y = 0; y < yButtons; ++y)
        {
            LabyrinthButton* button = new LabyrinthButton(EMPTY_SPACE, this);
            button->setGeometry(10+20*x,20+20*y,20,20);
            buttons[x][y] = button;
            button->setFocusPolicy(Qt::FocusPolicy::NoFocus);
            if ((x % 2 == 0 && y % 2 == 0) || x == 0 || y == 0 || x == xButtons - 1 || y == yButtons - 1)
                button->setText(BLOCKED_PATH);
            if (hidden)
                button->hide();
            else
                button->show();
            buttons[x][y] = button;
        }
    }

    int i = 0;
    for (uint8 x = 2; x < xButtons - 1; x+=2)
        for (uint8 y = 1; y < yButtons - 1; y+=2)
            buttons[x][y]->setText(QString(array[i++]));

    for (uint8 x = 1; x < xButtons - 1; x+=2)
        for (uint8 y = 2; y < yButtons - 1; y+=2)
            buttons[x][y]->setText(QString(array[i++]));

    showNormal(); // window mode
    resize(xButtons * 20 + 20, yButtons * 20 + 30);
    pos.x = 1;
    pos.y = 1;

    buttons[pos.x][pos.y]->setText(PLAYER_POSITION);
    buttons[xButtons - 2][yButtons - 2]->setText(FINISH);

    oldHidden = hidden;
    oldXButtons = xButtons; // for removal of old buttons
    oldYButtons = yButtons;

    if (hidden)
        LabyrinthTryChangePosition(0, 0); // show start

    timerStart = std::chrono::system_clock::now();
    if (!timerActive)
    {
        if (timerThread)
            timerThread->detach();

        timerActive = true;
        timerThread = std::make_unique<std::thread>(std::thread(&Labyrinth::Update,this));
    }
}

void Labyrinth::SetData(uint16 index, int32 value)
{
    switch (index)
    {
        case Data::SET_OPPONENT:
        {
            if (!opponent)
                opponent = new Opponent();
            opponent->socket = value;
            opponent->x = 1;
            opponent->y = 1;
            break;
        }
        case Data::SET_PLAYING:
        {
            if (value)
                QMetaObject::invokeMethod(this, "show");
            else
                mainWindow->StopGame(gameType);
            break;
        }
        default:
            break;
    }
}

SOCKET Labyrinth::GetChallenger()
{
    return opponent->socket;
}

uint8 Labyrinth::GetMaxX()
{
    return ui.sliderX->maximum();
}

uint8 Labyrinth::GetMaxY()
{
    return ui.sliderY->maximum();
}

void Labyrinth::TryConnectNeighboars(LabyrinthButton* button)
{
    uint8 x = button->x;
    uint8 y = button->y;

    if (x > 2 && buttons[x - 1][y]->text() != BLOCKED_PATH)
    {
        ConnectButton(buttons[x - 2][y], button);
    }
    if (x < xButtons - 3 && buttons[x + 1][y]->text() != BLOCKED_PATH)
    {
        ConnectButton(buttons[x + 2][y], button);
    }
    if (y > 2 && buttons[x][y - 1]->text() != BLOCKED_PATH)
    {
        ConnectButton(buttons[x][y - 2], button);
    }
    if (y < yButtons - 3 && buttons[x][y + 1]->text() != BLOCKED_PATH)
    {
        ConnectButton(buttons[x][y + 2], button);
    }
}

void Labyrinth::ConnectButton(LabyrinthButton* b, LabyrinthButton* old)
{
    if (b->isConnected)
        return;

    notChecked.push_back(b);
    disconnected.remove(b);
    b->isConnected = true;
    DEBUG_LOG("Connected " << int(b->x) << "," << int(b->y) << " from " << int(old->x) << "," << int(old->y) << "; connected = " << old->isConnected << std::endl);
}

std::list<Labyrinth::LabyrinthButton*> Labyrinth::GetNeighboarValidConnections(LabyrinthButton* button)
{
    uint8 x = button->x;
    uint8 y = button->y;
    std::list<Labyrinth::LabyrinthButton*> ret;

    if (x > 2 && buttons[x - 2][y]->isConnected)
        ret.push_back(buttons[x - 1][y]);

    if (x < xButtons - 2 && buttons[x + 2][y]->isConnected)
        ret.push_back(buttons[x + 1][y]);

    if (y > 2 && buttons[x][y - 2]->isConnected)
        ret.push_back(buttons[x][y - 1]);

    if (y < yButtons - 2 && buttons[x][y + 2]->isConnected)
        ret.push_back(buttons[x][y + 1]);
    return ret;
}

void Labyrinth::FixBlockedPaths()
{
    for (uint8 x = 1; x < xButtons - 1; x+=2)
        for (uint8 y = 1; y < yButtons - 1; y+=2)
            disconnected.push_back(buttons[x][y]);

    ConnectButton(buttons[1][1], buttons[1][1]);
    while (!disconnected.empty() || !notChecked.empty())
    {
        while (!notChecked.empty())
        {
            LabyrinthButton* b = *notChecked.begin();
            TryConnectNeighboars(b);
            notChecked.remove(b);
        }

        if (!disconnected.empty())
        {
            bool connected = false;
            std::list<LabyrinthButton*>::iterator itr = disconnected.begin();
            while (!connected)
            {
                LabyrinthButton* b = *itr;
                std::list<LabyrinthButton*> validNeighboars = GetNeighboarValidConnections(b);
                if (!validNeighboars.empty())
                {
                    auto itr = validNeighboars.begin();
                    if (validNeighboars.size() > 1) // pick random if size > 1
                        std::advance(itr, u_rand(0, validNeighboars.size() - 1));

                    DEBUG_LOG("Fixed connection for " << int(b->x) << "," << int(b->y) << " to " << int((*itr)->x) << "," << int((*itr)->y) << std::endl);
                    (*itr)->setText(EMPTY_SPACE);

                    ConnectButton(b, b);
                    connected = true;
                    itr = disconnected.begin();
                }
                else
                    ++itr;
            }
        }
    }
    if (disconnected.empty())
    {
        DEBUG_LOG("successfully generated, no need to fix anything" << std::endl);
    }
}

std::list<Labyrinth::LabyrinthButton*> Labyrinth::GetBlockablePositions(uint8 x, uint8 y)
{
    std::list<LabyrinthButton*> ret;
    if (buttons[x - 1][y]->text() == EMPTY_SPACE)
        ret.push_back(buttons[x - 1][y]);

    if (buttons[x + 1][y]->text() == EMPTY_SPACE)
        ret.push_back(buttons[x + 1][y]);

    if (buttons[x][y - 1]->text() == EMPTY_SPACE)
        ret.push_back(buttons[x][y - 1]);

    if (buttons[x][y + 1]->text() == EMPTY_SPACE)
        ret.push_back(buttons[x][y + 1]);

    return ret;
}

void Labyrinth::GenerateNewLabyrinthWorker()
{
    QMetaObject::invokeMethod(this, "GenerateNewLabyrinth");
}

void Labyrinth::GenerateNewLabyrinth()
{
    if (buttons)
    {
        movementMutex.lock();
        QObject::connect(buttons[0][0], &QAction::destroyed, this, &Labyrinth::GenerateNewLabyrinthWorker); // call this Fn from main Labyrinth thread
        ClearButtons();
        timerActive = false;
        return;
    }
    oldHidden = hidden;
    oldXButtons = xButtons; // for removal of old buttons
    oldYButtons = yButtons;

    buttons = new LabyrinthButton**[xButtons];
    for (uint8 x = 0; x < xButtons; ++x)
        buttons[x] = new LabyrinthButton*[yButtons];
    for (uint8 x = 0; x < xButtons; ++x)
    {
        for (uint8 y = 0; y < yButtons; ++y)
        {
            LabyrinthButton* button = new LabyrinthButton(EMPTY_SPACE, this);
            button->setGeometry(10+20*x,20+20*y,20,20);
            buttons[x][y] = button;
            button->setFocusPolicy(Qt::FocusPolicy::NoFocus);
            if ((x % 2 == 0 && y % 2 == 0) || x == 0 || y == 0 || x == xButtons - 1 || y == yButtons - 1)
                button->setText(BLOCKED_PATH);
            if (hidden)
                button->hide();
            else
                button->show();
            button->x = x;
            button->y = y;
        }
    }

    showNormal(); // window mode
    resize(xButtons * 20 + 20, yButtons * 20 + 30);
    pos.x = 1;
    pos.y = 1;
    buttons[pos.x][pos.y]->setText(PLAYER_POSITION);
    buttons[xButtons - 2][yButtons - 2]->setText(FINISH);

    for (uint8 x = 1; x < xButtons - 1; x+=2)
    {
        for (uint8 y = 1; y < yButtons - 1; y+=2)
        {
            std::list<LabyrinthButton*> blockable = GetBlockablePositions(x, y);

            if (blockable.size() < 2)
                continue;

            uint8 blocked = u_rand(0, blockable.size());
            for (uint8 i = 0; i < blocked; ++i)
            {
                auto itr = blockable.begin();
                if (blockable.size() > 1)
                    std::advance(itr, u_rand(0, blockable.size()));
                (*itr)->setText(BLOCKED_PATH);
                blockable.erase(itr);
            }
        }
    }

    FixBlockedPaths();
    if (hidden)
        LabyrinthTryChangePosition(0, 0); // show start

    if (opponent)
    {
        std::string gameArray;
        for (uint8 x = 2; x < xButtons - 1; x+=2)
            for (uint8 y = 1; y < yButtons - 1; y+=2)
                gameArray += buttons[x][y]->text().toStdString();

        for (uint8 x = 1; x < xButtons - 1; x+=2)
            for (uint8 y = 2; y < yButtons - 1; y+=2)
                gameArray += buttons[x][y]->text().toStdString();

        Message* msg = new Message(MessageType::LABYRINTH_GAME_SEND_ARRAY, gameArray.size() + 2 + sizeof(opponent->socket) + 1);
        *msg << opponent->socket;
        *msg << xButtons;
        *msg << yButtons;
        *msg << hidden;
        *msg << gameArray.c_str();
        msg->Send(socketfd);
    }
    movementMutex.unlock();
    timerStart = std::chrono::system_clock::now();
    if (!timerActive)
    {
        if (timerThread)
            timerThread->detach();

        timerActive = true;
        timerThread = std::make_unique<std::thread>(std::thread(&Labyrinth::Update,this));
    }
}

void Labyrinth::changeEvent(QEvent *e)
{
    GameWindow::changeEvent(e);
    switch (e->type())
    {
        case QEvent::LanguageChange:
            ui.retranslateUi(this);
            break;
        default:
            break;
    }
}

bool Labyrinth::LabyrinthTryChangePosition(int8 x, int8 y)
{
    if (buttons[pos.x + x][pos.y + y]->text() != BLOCKED_PATH) // not blocked
    {
        pos.x += x;
        pos.y += y;
    }
    else
        return false;

    if (buttons[pos.x][pos.y]->text() == FINISH)
        timerActive = false;

    if (hidden)
    {
        for (int8 x = pos.x -2; x < pos.x + 3; ++x)
        {
            for (int8 y = pos.y -2; y < pos.y + 3; ++y)
            {
                if (x < 0 || y < 0 || x >= oldXButtons || y >= oldYButtons)
                    continue;
                buttons[x][y]->show();
            }
        }
        buttons[xButtons - 2][yButtons - 2]->show();
    }
    return true;
}

void Labyrinth::keyPressEvent(QKeyEvent* e)
{
    if (e->isAutoRepeat())
        return;

    switch(e->key())
    {
        case 0x1000012: // left
            arrow ^= static_cast<uint8>(Arrows::LEFT);
            break;
        case 0x1000013: // up
            arrow ^= static_cast<uint8>(Arrows::UP);
            break;
        case 0x1000014: // right
            arrow ^= static_cast<uint8>(Arrows::RIGHT);
            break;
        case 0x1000015: // down
            arrow ^= static_cast<uint8>(Arrows::DOWN);
            break;
        default:
            break;
    }
    // update movement immediatedly on press, on arrow hold is updated in Labyrinth::Update
    if (arrow && timerActive)
        UpdateMovement();
}

void Labyrinth::keyReleaseEvent(QKeyEvent *e)
{
    if (e->isAutoRepeat())
        return;

    switch(e->key())
    {
        case 0x1000012: // left
            arrow ^= static_cast<uint8>(Arrows::LEFT);
            break;
        case 0x1000013: // up
            arrow ^= static_cast<uint8>(Arrows::UP);
            break;
        case 0x1000014: // right
            arrow ^= static_cast<uint8>(Arrows::RIGHT);
            break;
        case 0x1000015: // down
            arrow ^= static_cast<uint8>(Arrows::DOWN);
            break;
        default:
            break;
    }
}

template <typename T>
std::string to_string(T val, int precision)
{
    std::ostringstream out;
    out << std::fixed << std::setprecision(precision) << val;
    return out.str();
}

void Labyrinth::Update()
{
    // each time its value reach 10, timer is updated so its not updated every 5ms
    // this is required because of movement timer need better precision than 50ms
    uint8 timerUpdater = 0;
    while(timerActive)
    {
        if (++timerUpdater > 10)
        {
            UpdateTimer();
            timerUpdater = 0;
        }
        UpdateMovement();
        std::this_thread::sleep_for(5ms);
    }
}

void Labyrinth::UpdateMovement()
{
    if (!movementMutex.try_lock())
        return;

    if (std::chrono::system_clock::now() - lastPress > 150ms)
    {
        bool hasMoved = false;
        // left = 1000012, up 1000013, right 1000014 down 1000015
        if (opponent && opponent->x == pos.x && opponent->y == pos.y)
            buttons[pos.x][pos.y]->setText(OPPONENT_POSITION);
        else
            buttons[pos.x][pos.y]->setText(EMPTY_SPACE);

        if (arrow & static_cast<uint8>(Arrows::LEFT))
        {
            if (pos.x > 0)
                hasMoved = LabyrinthTryChangePosition(-1, 0);
        }
        else if (arrow & static_cast<uint8>(Arrows::UP))
        {
            if (pos.y > 0)
                hasMoved = LabyrinthTryChangePosition(0, -1);
        }
        else if (arrow & static_cast<uint8>(Arrows::RIGHT))
        {
            if (pos.x < xButtons)
                hasMoved = LabyrinthTryChangePosition(1, 0);
        }
        else if (arrow & static_cast<uint8>(Arrows::DOWN))
        {
            if (pos.y < yButtons)
                hasMoved = LabyrinthTryChangePosition(0, 1);
        }

        if (hasMoved)
        {
            if (opponent)
            {
                Message* msg = new Message(MessageType::LABYRINTH_GAME_SEND_MOVE, sizeof(opponent->socket) + 2 * sizeof(pos.x));
                *msg << opponent->socket;
                *msg << pos.x;
                *msg << pos.y;
                msg->Send(socketfd);
            }

            lastPress = std::chrono::system_clock::now();
        }

        if (opponent && opponent->x == pos.x && opponent->y == pos.y)
            buttons[pos.x][pos.y]->setText(OPPONENT_AND_PLAYER_POSITION);
        else
            buttons[pos.x][pos.y]->setText(PLAYER_POSITION);
    }
    movementMutex.unlock();
}

void Labyrinth::UpdateTimer()
{
        std::chrono::system_clock::duration time = std::chrono::system_clock::now() - timerStart;
        int64 us = time.count() / 100000000;
        float seconds = us / 10.0f;
        QMetaObject::invokeMethod(ui.timeLabel, "setText", Q_ARG(QString, to_string(seconds, 1).c_str()));
}

void Labyrinth::on_resetButton_clicked()
{
    if (!buttons)
        return;

    for (uint8 x = 0; x < oldXButtons; ++x)
        for (uint8 y = 0; y < oldYButtons; ++y)
            if (oldHidden)
                buttons[x][y]->hide();
            else
                buttons[x][y]->show();

    buttons[pos.x][pos.y]->setText(EMPTY_SPACE);
    pos.x = 1;
    pos.y = 1;
    buttons[pos.x][pos.y]->setText(PLAYER_POSITION);
    LabyrinthTryChangePosition(0, 0);
    timerStart = std::chrono::system_clock::now();
    if (!timerActive)
    {
        if (timerThread)
            timerThread->detach();

        timerActive = true;
        timerThread = std::make_unique<std::thread>(std::thread(&Labyrinth::Update,this));
    }
    buttons[oldXButtons - 2][oldYButtons - 2]->setText(FINISH);
    buttons[oldXButtons - 2][oldYButtons - 2]->show();
}

void Labyrinth::on_generateButton_clicked()
{
    GenerateNewLabyrinth();
}

void Labyrinth::on_sliderX_valueChanged(int value)
{
    xButtons = value;
    if (xButtons % 2 == 0)
        --xButtons;
}

void Labyrinth::on_sliderY_valueChanged(int value)
{
    yButtons = value;
    if (yButtons % 2 == 0)
        --yButtons;
}

void Labyrinth::on_hideButton_clicked()
{
    hidden = !hidden;
    if (hidden)
        ui.hideButton->setText("hidden");
    else
        ui.hideButton->setText("visible");
}
