#include "MessageHandler.h"
#include "Message.h"
#include "mainwindow.h"
#include "Timer.h"
#include "ui_mainwindow.h"
#include "config.h"
#include <iostream>
#include "GameWindow.h"
#include "Piskvorky.h"
#include "Labyrinth.h"

MessageHandler::MessageHandler(Ui::MainWindow* ui)
{
    using std::placeholders::_1;
    this->ui = ui;
    handler[int(MessageType::MSG_BROADCAST)] = std::bind(&MessageHandler::HandleMsgBroadcast, this, _1);
    handler[int(MessageType::MSG_WHISPER)] = std::bind(&MessageHandler::HandleMsgWhisper, this, _1);
    handler[int(MessageType::AUTH)] = std::bind(&MessageHandler::HandleAuth, this, _1);
    handler[int(MessageType::SET_AUTH_KEY)] = std::bind(&MessageHandler::HandleInvalid, this, _1);
    handler[int(MessageType::SET_NICKNAME)] = std::bind(&MessageHandler::HandleSetNickname, this, _1);
    handler[int(MessageType::NAME_QUERY)] = std::bind(&MessageHandler::HandleInvalid, this, _1);
    handler[int(MessageType::NAME_REFUSED)] = std::bind(&MessageHandler::HandleNameRefused, this, _1);
    handler[int(MessageType::CLIENT_DISCONNECTED)] = std::bind(&MessageHandler::HandleClientDisconnected, this, _1);
    handler[int(MessageType::ROLL)] = std::bind(&MessageHandler::HandleRoll, this, _1);
    handler[int(MessageType::TICTACTOE_CHALLENGE)] = std::bind(&MessageHandler::HandleTicTacToeChallenge, this, _1);
    handler[int(MessageType::TICTACTOE_START_GAME)] = std::bind(&MessageHandler::HandleTicTacToeStartGame, this, _1);
    handler[int(MessageType::LABYRINTH_CHALLENGE)] = std::bind(&MessageHandler::HandleLabyrinthChallenge, this, _1);
    handler[int(MessageType::LABYRINTH_START_GAME)] = std::bind(&MessageHandler::HandleLabyrinthStartGame, this, _1);
    handler[int(MessageType::TICTACTOE_MOVE)] = std::bind(&MessageHandler::HandleInvalid, this, _1);

}

void MessageHandler::HandleAuth(Message* msg)
{
    *msg >> server;
    std::string text = std::to_string(server);
    names[server].name = text;
    QMetaObject::invokeMethod(ui->messageEdit, "setEnabled", Q_ARG(bool, true));
    QMetaObject::invokeMethod(ui->messageEdit, "setText", Q_ARG(QString, ""));
}

void MessageHandler::HandleInvalid(Message* /*msg*/)
{
}

void MessageHandler::HandleMsgBroadcast(Message* msg)
{
    SOCKET id = 0;
    uint64 time = 0;
    uint8 colorlen = 0;
    std::string color;
    *msg >> id;
    *msg >> time;
    *msg >> colorlen;

    if (colorlen)
    {
        const char* color_cstr = msg->GetString(colorlen);
        color = color_cstr;
        delete[] color_cstr;
    }
    char* text = msg->GetString();
    if (names[id].name.empty())
    {
        MessageMap& msgs = names[id].messages;
        msgs[time] = new TextMessage(text, false, color);
        Message* msg = new Message(MessageType::NAME_QUERY, 4);
        *msg << id;
        msg->Send(socketfd);
    }
    else
        ShowGlobalMessage(text, id, time, color.c_str());
    color.clear();
}

void MessageHandler::HandleMsgWhisper(Message* msg)
{
    SOCKET id = 0;
    SOCKET receiver = 0;
    uint64 time = 0;
    uint8 colorlen = 0;
    std::string color;
    *msg >> receiver;
    *msg >> id;
    *msg >> time;
    *msg >> colorlen;

    if (colorlen)
        color = msg->GetString(colorlen);

    char* text = msg->GetString();
    if (names[id].name.empty())
    {
        MessageMap& msgs = names[id].messages;
        msgs[time] = new TextMessage(text, true, color);
        Message* msg = new Message(MessageType::NAME_QUERY, 4);
        *msg << id;
        msg->Send(socketfd);
    }
    else
        ShowPrivateMessage(text, receiver == server ? id : receiver, time, receiver != server, color.c_str());
}

void MessageHandler::HandleSetNickname(Message *msg)
{
    SOCKET socket = server;
    *msg >> socket;
    const char* name = msg->GetString();
    auto itr = names.find(socket);

    if (socket != server && itr != names.end())
    {
        QList<QListWidgetItem*> items = ui->clientsView->findItems(itr->second.name.c_str(), Qt::MatchExactly);
        if (items.begin() != items.end()) // should be always true..
            ui->clientsView->removeItemWidget(*items.begin());
        delete *items.begin();
        items.clear();
        itr->second.name = name;
    }
    else
        names[socket].name = std::string(name);

    MessageMap& texts = names[socket].messages;
    if (socket != server)
    {
        ui->clientsView->addItem(name);
        ui->clientsView->sortItems();
        if (!texts.empty())
        {
            for (auto itr = texts.begin();itr!=texts.end();++itr)
            {
                if (!itr->second->priv)
                    ShowGlobalMessage(itr->second->text.c_str(), socket, itr->first, itr->second->color.c_str());
                else
                    ShowPrivateMessage(itr->second->text.c_str(), socket, itr->first, itr->second->color.c_str());
                delete itr->second;
            }
            texts.clear();
        }
    }
    else if (!isdigit(name[0]))
        config->name = name;
}

void MessageHandler::HandleNameRefused(Message* /*msg*/)
{
    ShowError("Chosen nickname is already taken!");
}

void MessageHandler::HandleClientDisconnected(Message* msg)
{
    SOCKET socket;
    *msg >> socket;
    const char* name = names[socket].name.c_str();
    QList<QListWidgetItem*> items = ui->clientsView->findItems(name, Qt::MatchExactly);
    if (items.begin() != items.end()) // should be always true..
        ui->clientsView->removeItemWidget(*items.begin());
    for (auto itr : items)
        delete itr;
    names.erase(socket);
    items.clear();
}

void MessageHandler::HandleRoll(Message* msg)
{
    SOCKET socket = 0;
    char roll = 0;
    uint64 time = 0;
    *msg >> socket;
    *msg >> roll;
    *msg >> time;
    std::string rollstr = GetTimeOfTheDay(time);
    rollstr += ": " + names[socket].name + " rolls " + std::to_string(roll) + ".";
    ShowInfo(rollstr.c_str());
}

void MessageHandler::HandleTicTacToeChallenge(Message* msg)
{
    SOCKET challenger;
    SOCKET target;
    *msg >> target;
    *msg >> challenger;

    if (target == server)
    {
        GameWindow* game = ((MainWindow*)ui->centralWidget->parent())->GetGame(GameType::TICTACTOE);
        if (game && game->GetGameType() == GameType::TICTACTOE)
        {
            if (((Piskvorky*)game)->GetChallenger() == challenger)
            {
                Message* msg = new Message(MessageType::TICTACTOE_START_GAME, sizeof(SOCKET));
                *msg << challenger;
                uint16 len;
                const char* buffer = msg->ToPacket(len);
                send(socketfd, buffer, len, 0);
                return;
            }
        }
        ShowInfo(std::string(names[challenger].name + " has challenged you for a TicTacToe game.").c_str());
    }
    else
    {
        std::string text = "You have challenged ";
        text += names[target].name;
        text += std::string(" for a TicTacToe game.");
        ShowInfo(text.c_str());
    }
}

void MessageHandler::HandleTicTacToeStartGame(Message* msg)
{
    bool playingX;
    *msg >> playingX;
    GameWindow* game = ((MainWindow*)ui->centralWidget->parent())->GetGame(GameType::TICTACTOE);
    if (game && game->GetGameType() == GameType::TICTACTOE) // @TODO: move StartGame to SetData on playingX
        QMetaObject::invokeMethod(((Piskvorky*)game), "StartGame", Q_ARG(bool, playingX));
}

void MessageHandler::HandleLabyrinthChallenge(Message* msg)
{
    SOCKET challenger;
    SOCKET target;
    *msg >> target;
    *msg >> challenger;
    DEBUG_LOG(challenger << " " << target << " " << server <<  std::endl);
    if (target == server)
    {
        GameWindow* game = ((MainWindow*)ui->centralWidget->parent())->GetGame(GameType::LABYRINTH);
        if (game && game->GetGameType() == GameType::LABYRINTH)
        {
            // @TODO: GetChallenger -> GetData
            if (((Labyrinth*)game)->GetChallenger() == challenger)
            {
                Message* msg = new Message(MessageType::LABYRINTH_START_GAME, sizeof(SOCKET));
                *msg << challenger;
                msg->Send(socketfd);
                return;
            }
        }
        ShowInfo(std::string(names[challenger].name + " has challenged you for a Labyrinth game.").c_str());
    }
    else
    {
        std::string text = "You have challenged ";
        text += names[target].name;
        text += std::string(" for a Labyrinth game.");
        ShowInfo(text.c_str());
    }
}

void MessageHandler::HandleLabyrinthStartGame(Message* /*msg*/)
{
    GameWindow* game = ((MainWindow*)ui->centralWidget->parent())->GetGame(GameType::LABYRINTH);
    if (game && game->GetGameType() == GameType::LABYRINTH)
        game->SetData(Labyrinth::Data::SET_PLAYING, true);
}

void MessageHandler::ShowError(const char* msg)
{
    QString text = "<span style=\"background-color: red\">";
    text += msg;
    text += "</span>";
    QMetaObject::invokeMethod(ui->messageBrowser, "append", Q_ARG(QString, text));
}

void MessageHandler::ShowInfo(const char* msg)
{
    QString text = "<span style=\"background-color: yellow\">";
    text += msg;
    text += "</span>";
    QMetaObject::invokeMethod(ui->messageBrowser, "append", Q_ARG(QString, text));
}

void MessageHandler::ShowPrivateMessage(const char* msg, SOCKET id, uint64 time, bool sender, const char* color)
{
    ShowMessage(msg, id, time, color, "#00ff00", sender ? "PM TO " : "PM FROM ");
}

void MessageHandler::ShowGlobalMessage(const char* msg, SOCKET id, uint64 time, const char* color)
{
    ShowMessage(msg, id, time, color, "cyan");
}

void MessageHandler::ShowMessage(const char* msg, SOCKET id, uint64 time, const char* color, const char* bgcolor, const char* pretext)
{
    //QString text = QString("<a href=") + names[id].name.c_str() + "><span style=\"background-color: " + bgcolor + "\">";
    QString text = QString("<span style=\"background-color: ") + bgcolor + "\">" + pretext + "<a href=" + names[id].name.c_str() + ">";
    text += QString(names[id].name.c_str()) + " [" + GetTimeOfTheDay(time).c_str() + "]<br></a></span>";
    QString message = msg;
    message.replace("&", "&amp;");
    message.replace("\"", "&quot;");
    message.replace("<", "&lt;");
    message.replace(">", "&gt;");
    message.replace("\n", "<br>");
    message.replace(":D", "<img src=\"assets/bigsmile.png\"></img>");
    message.replace(":)", "<img src=\"assets/smile.png\"></img>");
    message.replace(":(", "<img src=\"assets/sad.png\"></img>");
    message.replace(":'(","<img src=\"assets/cry.png\"></img>");
    message.replace(";(", "<img src=\"assets/cry.png\"></img>");
    message.replace(":O", "<img src=\"assets/gasp.png\"></img>");
    message.replace(":P", "<img src=\"assets/tongue.png\"></img>");
    message.replace("(rofl)", "<img src=\"assets/rofl.png\"></img>");
    message.replace("'", "&apos;");
    text += QString("<font color = ") + QString(color) + QString(">");
    text += message;
    text += "</font>";
    QMetaObject::invokeMethod(ui->messageBrowser, "append", Q_ARG(QString, text));
}

std::string MessageHandler::format(int val)
{
    if (val < 10)
        return "0" + std::to_string(val);
    else
        return std::to_string(val);
}

std::string MessageHandler::GetTimeOfTheDay(uint64 timestamp)
{
    const time_t now = time_t(timestamp/1000);
    tm *tm_struct = localtime(&now);
    int y = tm_struct->tm_year + 1900;
    int month = tm_struct->tm_mon;
    int d = tm_struct->tm_mday;
    int h = tm_struct->tm_hour;
    int min = tm_struct->tm_min;
    int s = tm_struct->tm_sec;
    return std::to_string(y) + "-" + format(month) + "-" + format(d) + " " +format(h) + ":" + format(min) + ":" + format(s);
}
