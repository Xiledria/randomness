#include "config.h"
#include <fstream>
#include <iostream>
#include <QColor>

Config* config;

Config::Config()
{
    std::ifstream file("config.cfg");
    std::string str;
    while (std::getline(file, str))
        HandleLine(str);

    QColor c = fontcolor.c_str();
    if (!c.isValid())
    {
        std::cout << "Config color is not valid! Falling back to default." << std::endl;
        fontcolor = "#000000";
    }

    if (ip.empty())
    {
        std::cout << "No IP found in config.cfg, setting to localhost" << std::endl;
        ip = "127.0.0.1";
    }
}

Config::~Config()
{
    Save();
    ip.clear();
    name.clear();
    key.clear();
    fontcolor.clear();
    bgcolor.clear();
}

void Config::Save()
{
    std::ofstream file;
    file.open("config.cfg");
    std::string str;

    if (!ip.empty())
        str += "ip=" + ip + "\n";
    if (!name.empty())
        str += "name=" + name + "\n";
    if (!key.empty())
        str += "key=" + key + "\n";
    if (!fontcolor.empty())
        str += "fontcolor=" + fontcolor + "\n";

    file.write(str.c_str(), str.length());
    file.close();
}

void Config::HandleLine(std::string line)
{
    if (line[0] == '/' || line[0] == '-' || line[0] == '#')
        return;

    if (!line.find("ip=") && ip.empty()) // 0 = found on the begining of string
    {
        ip = line.c_str() + 3;
        std::cout << "ip = " << ip << std::endl;;
    }
    else if (!line.find("name=") && name.empty())
    {
        name = line.c_str() + 5;
        std::cout << "name = " << name << std::endl;;
    }
    else if (!line.find("key=") && key.empty())
    {
        key = line.c_str() + 4;
        std::cout << "key = " << key << std::endl;;
    }
    else if (!line.find("fontcolor=") && fontcolor.empty())
    {
        fontcolor = line.c_str() + 10;
        std::cout << "font color = " << fontcolor << std::endl;
    }
}
