#include "mainwindow.h"
#include <QApplication>
#include <QObjectUserData>
#include <QTextCodec>
#include <QTextFormat>
#include <QTextBlock>
#include <QTextCursor>

Q_DECLARE_METATYPE(QTextCharFormat)
Q_DECLARE_METATYPE(QTextBlock)
Q_DECLARE_METATYPE(QTextCursor)

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // register meta types
    {
        QTextCursor b;
        QTextCharFormat c;
        QTextBlock d;
        qRegisterMetaType(&c);
        qRegisterMetaType(&d);
        qRegisterMetaType(&b);
    }
    MainWindow w;
    w.show();
    return a.exec();
}
