#ifndef CONFIG_H
#define CONFIG_H
#include "SharedDefs.h"

class Config
{
private:
    void HandleLine(std::string line);
public:
    Config();
    ~Config();
    void Save();

    std::string ip;
    std::string name;
    std::string key;
    std::string fontcolor;
    std::string bgcolor;
};

extern Config* config;

#endif // CONFIG_H

