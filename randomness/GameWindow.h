#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <mainwindow.h>
#include <thread>
#include "SharedDefs.h"

class Message;

enum GameType
{
    TICTACTOE,
    TROLL_GAME,
    LABYRINTH,
    GAMES_MAX
};

class GameWindow : public QMainWindow
{
    Q_OBJECT
public:
    ~GameWindow();
    GameType GetGameType();
    void changeEvent(QEvent* e);
    explicit GameWindow(MainWindow* parent = 0);
protected:
    GameType gameType;
    MainWindow* mainWindow;
public slots:
    virtual void HandleMessage(Message* msg) = 0;
    virtual void SetData(uint16 index, int32 value) = 0;
signals:

public slots:
};

#endif // GAMEWINDOW_H
