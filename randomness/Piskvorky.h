#ifndef PISKVORKY_H
#define PISKVORKY_H

#include "GameWindow.h"
#include "ui_Piskvorky.h"
#include "SharedDefs.h"

class QPushButton;

class Piskvorky : public GameWindow
{
    Q_OBJECT
public:

    enum Data : uint16
    {
        SET_OPPONENT,
    };

    explicit Piskvorky(MainWindow *parent = 0);
    SOCKET GetChallenger();
    void HandleMessage(Message *msg) override;
    void SetData(uint16 index, int32 value) override;
    void EndGame(uint8 x, uint8 y);
    void OnClick();
public slots:
    void StartGame(bool playingX);
protected:
    void changeEvent(QEvent *e) override;
private:
    QPushButton* lastMove;
    bool playingX;
    bool isOnMove;
    SOCKET opponent;
    Ui::Piskvorky ui;
    QPushButton* buttons[20][20];
    const char* icons[2] =
    {
        "assets/bigsmile.png",
        "assets/cry.png"
    };
};

#endif // PISKVORKY_H
