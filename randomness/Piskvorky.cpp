#include "Piskvorky.h"
#include <thread>
#include <QPushButton>
#include <iostream>
#include "mainwindow.h"

Piskvorky::Piskvorky(MainWindow* parent) :
    GameWindow(parent)
{
    gameType = GameType::TICTACTOE;
    opponent = 0;
    lastMove = nullptr;
}

SOCKET Piskvorky::GetChallenger()
{
    return opponent;
}

void Piskvorky::StartGame(bool playingX)
{
    this->playingX = playingX;
    isOnMove = playingX;
    ui.setupUi(this);
    ui.turnLabel->setText(isOnMove? "It's your turn" : "Opponent playing");
    for (uint8 x = 0; x < 20; ++x)
    {
        for (uint8 y = 0; y < 20; ++y)
        {
            QPushButton* button = new QPushButton(" ", this);
            button->setGeometry(10+20*x,10+20*y,20,20);
            buttons[x][y] = button;
            button->setFocusPolicy(Qt::FocusPolicy::NoFocus);
            button->show();
            QObject::connect(button, &QPushButton::clicked, this, &Piskvorky::OnClick);
        }
    }
    show();
}

void Piskvorky::changeEvent(QEvent* e)
{
    GameWindow::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            ui.retranslateUi(this);
            break;
        default:
            break;
    }
}

void Piskvorky::OnClick()
{
    if (!isOnMove)
         return;
    QPushButton* button = (QPushButton*)sender();
    if (button->text() != QString(" "))
        return;

    uint8 x = (button->pos().x() + 10) / 20;
    uint8 y = (button->pos().y() + 10) / 20;
    Message* msg = new Message(MessageType::TICTACTOE_MOVE, 2 + sizeof(opponent));
    *msg << opponent;
    *msg << x;
    *msg << y;
    uint16 len;
    const char* buffer = msg->ToPacket(len);
    send(socketfd, buffer, len, 0);
}

void Piskvorky::SetData(uint16 index, int32 value)
{
    switch(index)
    {
        case Data::SET_OPPONENT:
            opponent = value;
            break;
        default:
            break;
    }
}

void Piskvorky::HandleMessage(Message* msg)
{
    switch(msg->GetId())
    {
        case MessageType::TICTACTOE_MOVE:
        {
            uint8 x = 0;
            uint8 y = 0;
            SOCKET client = 0;
            *msg >> client;
            *msg >> x;
            *msg >> y;
            --x; --y;
            bool showX;
            if (client == server)
                showX = playingX;
             else
                showX = !playingX;
            isOnMove = !isOnMove;
            ui.turnLabel->setText(isOnMove? "It's your turn" : "Opponent playing");
            lastMove = buttons[x][y];
            lastMove->setIcon(QIcon(icons[showX ? 0 : 1]));
            lastMove->setText(showX ? "X" : "O");
            /* tady můžeš zapnout třeba tu tvojí funkci, vždy když  ti příjde novej tah :D*/
            EndGame(x, y);  // takže ty tahy co máš v týhle funkci předáš jako vstupní parametry do tvojí funkce
            break;
        }
        default:
            break;
    }
}

void Piskvorky::EndGame(uint8 x, uint8 y)
{

    switch(lastMove->text()[0].cell()) // ono to vrací nějakej retard znak, kterej není char ale QChar protože to je retardovaná class z Qt :D tak si vytvořili vlastní datovej typ a musíš vždy získávat normal char z toho Q_Q
    {
        case 'X':
        {
            uint8 combo;
            if (buttons[x+1][y]->text() == "X")
            {
                if (buttons[x+2][y]->text() == "X")
                {
                    if (buttons[x+3][y]->text() == "X")
                    {
                        if (buttons[x+4][y]->text() == "X")
                        {
                            combo = 5;
                        }
                        else combo = 4;
                    }
                    else combo = 3;
                }
                else combo = 2;
            }
            else combo = 1;

            if (combo < 5)
            {
                if (buttons[x-1][y]->text() == "X")
                {
                    if (buttons[x-2][y]->text() == "X")
                    {
                        if (buttons[x-3][y]->text() == "X")
                        {
                            if (buttons[x-4][y]->text() == "X")
                            {
                                combo += 4;
                            }
                            else combo += 3;
                        }
                        else combo += 2;
                    }
                    else combo += 1;
                }
            }
            if (combo >= 5)
            {
                if (playingX == true)
                {
                    std::cout << "GG" << std::endl;
                }

                else if (playingX == false)
                {
                    std::cout << "WP" << std::endl;
                }
            }
            break;
        }
        case 'O':
            break;
    } // si to můžeš otestovat :D
}

