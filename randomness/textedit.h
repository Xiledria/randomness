#ifndef TEXTEDIT_H
#define TEXTEDIT_H

#include <QTextEdit>
#include "mainwindow.h"

class textedit : public QTextEdit
{
    Q_OBJECT
public:
    explicit textedit(QWidget *parent = 0);
protected:
    void keyPressEvent(QKeyEvent *e);
signals:
public slots:

};

#endif // TEXTEDIT_H
