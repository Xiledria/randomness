#include "TrollGame.h"
#include <iostream>

TrollGame::TrollGame(MainWindow* parent) :
    GameWindow(parent)
{
    gameType = GameType::TROLL_GAME;
    ui.setupUi(this);
    show();
}

void TrollGame::changeEvent(QEvent *e)
{
    GameWindow::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            ui.retranslateUi(this);
            break;
        default:
            break;
    }
}

void TrollGame::HandleMessage(Message* /*msg*/)
{

}

void TrollGame::SetData(uint16 /*index*/, int32 /*value*/)
{
}
