#ifndef LABYRINTH_H
#define LABYRINTH_H

#include "ui_Labyrinth.h"
#include "GameWindow.h"
#include <list>
#include <iostream>
#include <mutex>

class QPushButton;

class Labyrinth : public GameWindow
{
    Q_OBJECT
    class LabyrinthButton;
public:

#define EMPTY_SPACE " "
#define BLOCKED_PATH "x"
#define FINISH "F"
#define PLAYER_POSITION "o"
#define OPPONENT_POSITION "q"
#define OPPONENT_AND_PLAYER_POSITION "qo"

    enum Data : uint16
    {
        SET_OPPONENT,
        SET_PLAYING,
        SET_ARRAY,
    };

    explicit Labyrinth(MainWindow* parent = 0);
    ~Labyrinth();
    void HandleMessage(Message* msg) override;
    void SetData(uint16 index, int32 value) override;
    SOCKET GetChallenger();
    uint8 GetMaxX();
    uint8 GetMaxY();
protected:
    void changeEvent(QEvent* e);
    bool LabyrinthTryChangePosition(int8 x, int8 y);
    void GenerateNewLabyrinthWorker();
    void TryConnectNeighboars(LabyrinthButton* button);
    void ConnectButton(LabyrinthButton* button, LabyrinthButton* old);
    std::list<LabyrinthButton*> GetNeighboarValidConnections(LabyrinthButton* button);
    void FixBlockedPaths();
    std::list<LabyrinthButton*> GetBlockablePositions(uint8 x, uint8 y);
    void keyPressEvent(QKeyEvent* e) override;
    void keyReleaseEvent(QKeyEvent* e) override;
    void Update();
    void UpdateTimer();
    void UpdateMovement();
private slots:
    void ClearButtons();
    void GenerateNewLabyrinth();
    void on_resetButton_clicked();
    void on_generateButton_clicked();
    void on_sliderX_valueChanged(int value);
    void on_sliderY_valueChanged(int value);
    void on_hideButton_clicked();

private:
    Ui::Labyrinth ui;

    class LabyrinthButton : public QPushButton
    {
    public:
        LabyrinthButton(QString text, QWidget* parent) : QPushButton(text, parent)
        {
            QObject::connect(this, &LabyrinthButton::clicked, this, &LabyrinthButton::OnClick);
            isConnected = false;
        }

        void OnClick()
        {
            DEBUG_LOG(int(x) << "," << int(y) << std::endl);
        }

        bool isConnected;
        uint8 x;
        uint8 y;
    };

    LabyrinthButton*** buttons;
    std::list<LabyrinthButton*> notChecked;   // used for checking valid connections between labyrinth parts
    std::list<LabyrinthButton*> disconnected; // used for checking valid connections between labyrinth parts
    uint8 xButtons;
    uint8 yButtons;
    uint8 oldXButtons;
    uint8 oldYButtons;
    std::mutex movementMutex;
    enum class Arrows : uint8
    {
        LEFT  = 0x01,
        UP    = 0x02,
        RIGHT = 0x04,
        DOWN  = 0x08,
    };

    uint8 arrow;
    struct Position
    {
        uint8 x;
        uint8 y;
    };
    std::chrono::system_clock::time_point lastPress;
    std::chrono::system_clock::time_point timerStart;
    bool timerActive;
    bool hidden;
    bool oldHidden;
    Position pos;
    std::unique_ptr<std::thread> timerThread;

    // multiplayer stuff
    struct Opponent
    {
        SOCKET socket;
        uint8 maxX;
        uint8 maxY;
        uint8 x;
        uint8 y;
    };

private:
    Opponent* opponent;
private slots:
        void HandleReceivedArray(QString array);
};

#endif // LABYRINTH_H
