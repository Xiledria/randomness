#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "textedit.h"
#include "QTextEdit"
#include <iostream>
#include <Network.h>
#include <QScrollBar>
#include <ctime>
#include <config.h>
#include "Timer.h"
#include <QInputDialog>
#include <QColorDialog>
#include "MessageHandler.h"
#include "GameWindow.h"
#include "Piskvorky.h"
#include "TrollGame.h"
#include "Labyrinth.h"

std::map<SOCKET, Host> names;
SOCKET server;
SOCKET socketfd;
bool isConnected;
char* rcvbuffer;
struct addrinfo host_info; // server
struct addrinfo *host_info_list;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->messageBrowser->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    config = new Config();
    SetDisconnected();
    ui->messageBrowser->setFocusPolicy(Qt::NoFocus);
    msgHandler = new MessageHandler(ui);
    QObject::connect(ui->settingsFontcolor, &QAction::triggered, this, &MainWindow::HandleColorSettings);
    QObject::connect(ui->settingsName, &QAction::triggered, this, &MainWindow::HandleNameSettings);
    QObject::connect(ui->settingsIP, &QAction::triggered, this, &MainWindow::HandleIPSettings);
    QObject::connect(ui->gameTicTacToe, &QAction::triggered, this, &MainWindow::HandleGameTicTacToe);
    QObject::connect(ui->gameTrollGame, &QAction::triggered, this, &MainWindow::HandleTrollGame);
    QObject::connect(ui->gameLabyrinth, &QAction::triggered, this, &MainWindow::HandleLabyrinth);
    stopped = false;
    host_info_list = nullptr;
    netThrd = new std::thread(&MainWindow::RecvMsg,this);
}

MainWindow::~MainWindow()
{
    SetDisconnected();
    stopped = true;
    ::closesocket(socketfd);
    netThrd->detach();
    games.clear();
    if (host_info_list)
        free(host_info_list);

    delete[] rcvbuffer;
    delete netThrd;
    delete config;
    delete msgHandler;
    delete ui;
}

GameWindow* MainWindow::GetGame(uint16 id)
{

    return games.find(id) != games.end() ? games[id] : nullptr;
}

void MainWindow::SetGame(GameWindow* game)
{
    games[game->GetGameType()] = game;
}

void MainWindow::StopGame(uint16 id, bool freePtr)
{
    games[id]->deleteLater();
    if (freePtr)
        games.erase(id);
}

void MainWindow::ConnectToServer()
{
#if defined(_WIN32)
    WSADATA wsaData;
    int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (result)
        return;
#endif
    DEBUG_LOG("Connecting to " << config->ip << std::endl);
    int status = 0;
    struct addrinfo host_info;
    if (host_info_list)
        free(host_info_list);
    host_info_list = nullptr;
    memset(&host_info, 0, sizeof host_info);
    host_info.ai_family = PF_INET;
    host_info.ai_socktype = SOCK_STREAM;
    host_info.ai_protocol = IPPROTO_TCP;
    status = getaddrinfo(config->ip.c_str(), SERVER_PORT, &host_info, &host_info_list);


    if (status != 0)
    {
        DEBUG_LOG("getaddrinfo error" << gai_strerror(status) << std::endl);
        return;
    }

    // if socket was not successfully initialised
    ::closesocket(socketfd);
        socketfd = 0;

    socketfd = socket(host_info_list->ai_family, host_info_list->ai_socktype,
                      host_info_list->ai_protocol);

    if (IsSocketError(socketfd))
    {
        DEBUG_LOG("socket error " << std::endl);
        return;
    }

    status = ::connect(socketfd, host_info_list->ai_addr, int(host_info_list->ai_addrlen));
    if (status == -1)
    {
        DEBUG_LOG("connect error" << std::endl);
        return;
    }

    // send authentication packet
    {
        Message* msg = new Message(MessageType::AUTH, config->key.size());
        if (!config->key.empty())
            *msg << config->key;
        msg->Send(socketfd);
    }

    if (config->key.empty()) // if nick is already registered, just ignore changing name
    {
        DEBUG_LOG("connected" << std::endl);
        if (!config->name.empty())
        {
            Message* msg = new Message(MessageType::SET_NICKNAME, config->name.size());
            *msg << config->name.c_str();
            msg->Send(socketfd);
        }
    }
    isConnected = true;
}

void MainWindow::RecvMsg()
{
    ssize_t rcvbytes;
    std::deque<Message*> messages;
    rcvbuffer = new char[65535];
    while (!stopped)
    {
        if (!isConnected)
        {
            ConnectToServer();
            wait(5);
            continue;
        }

        rcvbytes = recv(socketfd, rcvbuffer, 65535/*max bytes*/, 0);
        if (rcvbytes == -1 || rcvbytes == 0)
        {
            DEBUG_LOG("Server stopped" << std::endl);
            SetDisconnected();
        }
        else
        {
            NetworkConnection::GetMessages(messages, rcvbytes, rcvbuffer);
            while (!messages.empty())
            {
                Message* msg = messages.front();
                if (msg->GetId() >= MessageType::MESSAGES_START)
                {
                    if (msg->GetId() < MessageType::GAMES_START)
                        (*msgHandler)[msg->GetId()](msg);
                    else
                    {
                        GameType id = GAMES_MAX;
                        if (msg->GetId() < MessageType::TICTACTOE_END)
                            id = GameType::TICTACTOE;
                        else if (msg->GetId() < MessageType::TROLL_GAME_END)
                            id = GameType::TROLL_GAME;
                        else if (msg->GetId() < MessageType::LABYRINTH_GAME_END)
                            id = GameType::LABYRINTH;

                        games[id]->HandleMessage(msg);
                    }

                }
                messages.erase(messages.begin());
            }
        }
    }
}

void MainWindow::SetDisconnected()
{
    QMetaObject::invokeMethod(ui->messageEdit, "setEnabled", Q_ARG(bool, false));
    QMetaObject::invokeMethod(ui->messageEdit, "setText", Q_ARG(QString, "Disconnected"));
    QMetaObject::invokeMethod(ui->clientsView, "clear");
    names.clear();
    isConnected = false;
}

void MainWindow::HandleColorSettings()
{
    QColor color;
    if (config->fontcolor.empty())
        color = QColor(Qt::white);
    else
        color = QColor(config->fontcolor.c_str());
    color = QColorDialog::getColor(color, nullptr, "Select your font color");
    if (color.isValid())
        config->fontcolor = color.name().toStdString();
}

void MainWindow::HandleIPSettings()
{
    QString text = QInputDialog::getText(nullptr, "Type server IP address", "", QLineEdit::Normal, config->ip.c_str());
    config->ip = text.toStdString();
    if (!socketfd)
        return;

    send(socketfd, " ", 1, 0); // send info about closed connection
    ::closesocket(socketfd);
}

void MainWindow::HandleNameSettings()
{
    QString text = QInputDialog::getText(nullptr, "Choose your nickname", "", QLineEdit::Normal, config->name.c_str());
    if (text.isEmpty() || text[0].isDigit() || text[0].isSpace())
        return;

    if (isConnected)
    {
        Message* msg = new Message(MessageType::SET_NICKNAME,text.toStdString().size());
        *msg << text.toStdString();
        msg->Send(socketfd);
    }
    else
        config->name = text.toStdString();
}

void MainWindow::HandleGameTicTacToe()
{
    /*if (game)
    {
        if (game->baseSize().isValid())
        {
            msgHandler->ShowInfo("Close previous game first!");
            return;
        }
        else
        {
            delete game;
            game = nullptr;
        }
    }
    game = new Piskvorky(nullptr);*/
}

void MainWindow::HandleTrollGame()
{
    auto itr = games.find(GameType::TROLL_GAME);
    if (itr != games.end())
    {
        msgHandler->ShowInfo("Close previous game first!");
        return;
    }
    games[GameType::TROLL_GAME] = new TrollGame(this);
}

void MainWindow::HandleLabyrinth()
{
    auto itr = games.find(GameType::LABYRINTH);
    if (itr != games.end())
    {
        msgHandler->ShowInfo("Close previous game first!");
        return;
    }
    games[GameType::LABYRINTH] = new Labyrinth(this);
    games[GameType::LABYRINTH]->SetData(Labyrinth::Data::SET_PLAYING, true);
}

void MainWindow::on_messageBrowser_anchorClicked(const QUrl &arg1)
{
    QMetaObject::invokeMethod(ui->messageEdit, "setText", Q_ARG(QString, ""));
    QMetaObject::invokeMethod(ui->messageEdit, "append", Q_ARG(QString, QString("/w ") + arg1.toString() + " "));
    QMetaObject::invokeMethod(ui->messageEdit, "setFocus");
}

void MainWindow::on_clientsView_itemDoubleClicked(QListWidgetItem *item)
{
    Qt::MouseButtons buttons = qApp->mouseButtons();
    if (buttons == Qt::MouseButton::LeftButton)
    {
        QMetaObject::invokeMethod(ui->messageEdit, "setText", Q_ARG(QString, ""));
        QMetaObject::invokeMethod(ui->messageEdit, "append", Q_ARG(QString, QString("/w ") + item->text() + " "));
        QMetaObject::invokeMethod(ui->messageEdit, "setFocus");
    }
}


void MainWindow::on_clientsView_itemPressed(QListWidgetItem* /*item*/)
{
    Qt::MouseButtons buttons = qApp->mouseButtons();
    if (buttons == Qt::MouseButton::RightButton)
    {
        QPoint pos = mapFromGlobal(QCursor::pos());
        QMenu contextMenu("Context menu", this);
        QAction* action = new QAction("Whisper", this);
        contextMenu.addAction(action);
        QObject::connect(action, &QAction::triggered, this, &MainWindow::HandleRightClickClientWhisper);
        action = new QAction("Play TicTacToe", this);
        contextMenu.addAction(action);
        QObject::connect(action, &QAction::triggered, this, &MainWindow::HandleRightClickClientTicTacToe);
        action = new QAction("Play Labyrinth", this);
        contextMenu.addAction(action);
        QObject::connect(action, &QAction::triggered, this, &MainWindow::HandleRightClickLabyrinth);
        contextMenu.exec(mapToGlobal(pos));
    }

}

void MainWindow::HandleRightClickClientWhisper()
{
    QMetaObject::invokeMethod(ui->messageEdit, "setText", Q_ARG(QString, ""));
    QMetaObject::invokeMethod(ui->messageEdit, "append", Q_ARG(QString, QString("/w ") + ui->clientsView->currentItem()->text() + " "));
    QMetaObject::invokeMethod(ui->messageEdit, "setFocus");
}

void MainWindow::HandleRightClickClientTicTacToe()
{
    SOCKET opponent = 0;
    std::string name = ui->clientsView->currentItem()->text().toStdString();
    for (auto itr : names)
    {
        if (itr.second.name != name)
            continue;

        opponent = itr.first;
        break;
    }

    if (!opponent) // should never happend
        return;

    if (games.find(GameType::TICTACTOE) == games.end())
        games[GameType::TICTACTOE] = new Piskvorky(this);

    games[GameType::TICTACTOE]->SetData(Piskvorky::Data::SET_OPPONENT, opponent);

    Message* msg = new Message(MessageType::TICTACTOE_CHALLENGE, sizeof(SOCKET));
    *msg << opponent;
    msg->Send(socketfd);
}

void MainWindow::HandleRightClickLabyrinth()
{
    SOCKET opponent = 0;
    std::string name = ui->clientsView->currentItem()->text().toStdString();
    for (auto itr : names)
    {
        if (itr.second.name != name)
            continue;

        opponent = itr.first;
        break;
    }

    if (!opponent) // should never happend
        return;

    if (games.find(GameType::LABYRINTH) == games.end())
        games[GameType::LABYRINTH] = new Labyrinth(this);

    games[GameType::LABYRINTH]->SetData(Labyrinth::Data::SET_OPPONENT, opponent);


    Message* msg = new Message(MessageType::LABYRINTH_CHALLENGE, sizeof(SOCKET));
    *msg << opponent;
    msg->Send(socketfd);
}

void MainWindow::on_RollButton_pressed()
{
    Message* msg = new Message(MessageType::ROLL, 0);
    msg->Send(socketfd);
}

void MainWindow::on_RegisterButton_clicked()
{
    Message* msg = new Message(MessageType::AUTH_REGISTER_NICK, config->name.size());
    *msg << config->name.c_str();
    msg->Send(socketfd);
}
