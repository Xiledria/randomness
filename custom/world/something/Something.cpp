#include "Something.h"


Something::Something()
{
    id = 0;
}

Human* Something::FindHuman(uint64 id)
{
    std::map<uint64,Human*>::iterator itr = humans.find(id);
    if (itr != humans.end())
        return itr->second;
    return NULL;
}

void Something::RemoveHuman(Human* human)
{
    humans.erase(human->GetId());
}

void Something::AddHuman(Human* human)
{
    humans.insert(std::pair<uint64,Human*>(human->GetId(),human));
}