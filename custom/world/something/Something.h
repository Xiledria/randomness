#ifndef __SOMETHING_H
#define __SOMETHING_H

#include "SharedDefs.h"
#include "Singleton.inl"
#include <map>
#include "Human.h"


//allows to access game objects
class Something
{
public:
    Something();
    ~Something(){}

    std::map<uint64,Human*> GetHumans() { return humans; }
    Human* FindHuman(uint64 id);

    void AddHuman(Human* human);
    void RemoveHuman(Human* human);
    uint64 GenerateId()   { return ++id; }

protected:
    uint64                id;

    std::map<uint64,Human*> humans;
};

#define something Singleton<Something>::Instance()
#endif