#ifndef __HUMAN_H
#define __HUMAN_H

#include "SharedDefs.h"
#include "Message.h"
#include <map>
#include <thread>
#include <list>
#include <mutex>
#include <cstdio>

class Human
{
public:
    Human(Gender gender, std::string name, bool initThrd = true);
    ~Human();
    virtual void Die()  { deathState = DEATHSTATE_DEAD; }
    bool isAlive() { return !deathState; }
    void _KissEffect(Human* who);
    void Kiss(Human* who);
    void SetLoved(Human* lovedOne);
    std::list<Human*> GetHumansByRelations(Relation min, Relation max);
    void SetHappiness(int8 happiness);
    int8 GetHappiness() { return happiness; }
    Relation GetRelation(Human* who);
    void SetRelation(Human* who, Relation relation);
    Gender GetGender() { return gender; }
    // value < 0: aversion
    // value = 0: not affected
    // value > 0: satisfied
    void SetOrientation(Gender orientation,int8 value) { this->orientation[orientation] = value; }
    std::map<uint64,Relation> GetRelations() { return relations; }
    uint64 GetId() { return id; }
    std::string GetName() { return name; }
    TypeId GetTypeId() { return typeId; }
    std::thread* GetThread() { return _thrd; }
    void AddThreadMessage(Message* message) { _thrdMsgMtx.lock(); _messages.push_back(message); _thrdMsgMtx.unlock(); }
protected:
    Gender gender;
    DeathState deathState;
    int8 happiness;
    uint64 id;
    TypeId typeId;
    std::string name;
    // value < 0: aversion
    // value = 0: not affected
    // value > 0: satisfied
    int8* orientation;
    std::map<uint64,Relation> relations;

    std::mutex _thrdMsgMtx;
    std::thread* _thrd;
    std::list<Message*> _messages;
    void HandleMessage(Message* message);
    void InitThread();
    virtual void KeepAlive();
};

#endif
