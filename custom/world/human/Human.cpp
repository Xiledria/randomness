#include "Human.h"
#include "Something.h"
#include "Timer.h"


Human::Human(Gender gender,std::string name,  bool initThrd)
{
    this->name = name;
    this->gender = gender;
    id = something->GenerateId();
    happiness = 0;
    orientation = new int8[2];
    if (gender == GENDER_MALE)
    {
        orientation[GENDER_MALE]  = -1;
        orientation[GENDER_FEMALE] = 1;
    }
    else
    {
        orientation[GENDER_MALE]   = 1;
        orientation[GENDER_FEMALE] = 0;
    }
    if (!typeId) // just to prevent overriding typeId from child classes
        typeId = TYPEID_HUMAN;
    something->AddHuman(this);
    if (initThrd)
        _thrd = new std::thread(&Human::KeepAlive,this);
}

Human::~Human()
{
    _thrd->detach();
    something->RemoveHuman(this);
}

void Human::InitThread()
{
    KeepAlive();
}

void Human::KeepAlive()
{
    for(;;)
    {
        wait(50);
        _thrdMsgMtx.lock();
        if (!_messages.empty())
            HandleMessage(*_messages.begin());
        _thrdMsgMtx.unlock();
    }
}

void Human::HandleMessage(Message* message)
{
    if (message->GetId() < MessageType::MESSAGES_END)
    {

    }
    else
        printf("INVALID MESSAGE TYPE: id = %u\n",message->GetId());

    _messages.remove(message);
}

Relation Human::GetRelation(Human* who)
{
    if (!relations.empty())
    {
        std::map<uint64,Relation>::iterator itr = relations.find(who->GetId());
        if (itr != relations.end())
            return itr->second;
    }
    return RELATION_NEUTRAL; // not found -> neutral
}

void Human::SetRelation(Human* who, Relation relation)
{
    relations[who->GetId()] = relation;
}

std::list<Human*> Human::GetHumansByRelations(Relation min, Relation max)
{
    std::list<Human*> _list;
    for (std::map<uint64,Relation>::iterator itr = relations.begin(); itr != relations.end(); ++itr)
    {
        Relation relation = itr->second;
        if (relation >= min && relation <= max)
        {
            if (Human* found = something->FindHuman(itr->first))
            {
                _list.push_back(found);
            }
        }
    }
    return _list;
}

void Human::SetLoved(Human* loved)
{
    if (!relations.empty())
    {
        std::map<uint64,Relation>::iterator itr = relations.find(loved->GetId());
        if (itr != relations.end())
            if (itr->second < RELATION_LOVE)
            {
                SetRelation(loved,RELATION_LOVE);
                return;
            }
    }
    SetRelation(loved,RELATION_LOVE);
}

void Human::SetHappiness(int8 happiness)
{
    if (happiness > 100)
        happiness = 100;
    else if (happiness < -100)
        happiness = -100;
    this->happiness = happiness;
}

void Human::_KissEffect(Human* who)
{
    Relation relation = GetRelation(who);
    int8 orient = orientation[who->GetGender()];
    if (relation > RELATION_FRIENDLY && orient > 0)
        SetHappiness(GetHappiness()+orient);
    else if (orient<0)
        SetHappiness(GetHappiness()-orient);
    else if (relation < RELATION_FRIENDLY)
        SetHappiness(GetHappiness()+((relation-50)/10));
}

void Human::Kiss(Human* /*who*/)
{/*
    SetHappiness(GetHappiness()+1);
    _KissEffect(who);
    Message* message = new Message(MessageType::KISS, sizeof(this));
    *message << this;
    who->AddThreadMessage(message);*/
}
