#include "Network.h"
#include "cssrt.h"
#include "Timer.h"
#include <cstdio>
#include <iostream>
#include <sstream>
#include <deque>
#include <fstream>

NetworkConnection::NetworkConnection()
{
    std::ifstream file("nick.reg");
    std::string str;
    while (std::getline(file, str))
    {
        size_t delimiter = str.find("*",0);
        if (delimiter != str.npos)
        {
            std::string nick = str.substr(0, delimiter);
            std::string key = str.substr(delimiter + 1, str.size());
            registeredNicknames[nick] = key;
        }
    }
    file.close();
    _thread = new std::thread(&NetworkConnection::Initialize,this);
}

NetworkConnection::~NetworkConnection()
{
    if (!connections.empty())
        for (auto itr = connections.begin();itr != connections.end(); ++itr)
        {
            itr->second->thrd->detach();
            itr->second->thrd->~thread();
            delete itr->second;
        }
    connections.clear();
    delete _info;

    registeredNicknames.clear();
}

void NetworkConnection::Initialize()
{
#if defined(_WIN32)
    WSADATA wsaData;
    assert(WSAStartup(MAKEWORD(2, 2), &wsaData) == 0);
#endif
    _info = new addrinfo();
    _info->ai_family = AF_INET;
    _info->ai_socktype = SOCK_STREAM;
    _info->ai_flags = AI_PASSIVE;
    _info->ai_protocol = IPPROTO_TCP;
    assert(getaddrinfo(NULL, SERVER_PORT, _info, &_info) == 0);
    _socketfd = socket(_info->ai_family, _info->ai_socktype, _info->ai_protocol);
    assert(!IsSocketError(_socketfd));
    int opt = 1;
    assert(setsockopt(_socketfd, SOL_SOCKET, SO_REUSEADDR, (char*)&opt, sizeof(opt)) != -1);

    assert(bind(_socketfd, _info->ai_addr, int32(_info->ai_addrlen)) != -1);
    assert(listen(_socketfd, 5) != -1);
    ListenToConnections();
}

void NetworkConnection::GetMessages(std::deque<Message*>& messages, ssize_t rcvbytes, char* rcvbuffer)
{
    uint16 offset = 0; // offsetted position in rcvbuffer
    while (rcvbytes > 1)
    {
        uint16 messageBytes; // bytes for single message
        memcpy((uint8*)&messageBytes, rcvbuffer + offset, sizeof(messageBytes));
        offset += Message::GetBufferByteSize();
        char* messageBuffer = new char[messageBytes + 1];
        memcpy(messageBuffer, rcvbuffer + offset, messageBytes);
        messageBuffer[messageBytes] = 0;
        Message* message = new Message(messageBuffer, messageBytes);
        messages.push_back(message);
        offset += messageBytes;
        rcvbytes -= messageBytes + Message::GetBufferByteSize();
        delete[] messageBuffer;
    }
}

void NetworkConnection::HandleConnection(SOCKET client)
{
    ssize_t rcvbytes;
    std::deque<Message*> messages;
    char* rcvbuffer = new char[65535];
    while (true)
    {
        rcvbytes = recv(client, rcvbuffer,65535/*max bytes*/, 0);
        if (rcvbytes < 2) // -1 = error, 0 = closed, 1 = closed manually
        {
            connectionMtx.lock();
            closesocket(client);
            auto itr = connections.find(client);
            if (itr != connections.end())
                connections.erase(itr);
            connectionMtx.unlock();

            Message* msg = new Message(MessageType::CLIENT_DISCONNECTED, 4);
            *msg << client;
            Broadcast(msg, client);
            delete msg;
            return;
        }
        else
        {
            GetMessages(messages, rcvbytes, rcvbuffer);
            while (!messages.empty())
            {
                Message* msg = messages.front();
                switch(msg->GetId())
                {
                    case MessageType::MSG_BROADCAST:
                    {
                        Broadcast(msg, 0, client);
                        break;
                    }
                    case MessageType::MSG_WHISPER:
                    {
                        SOCKET receiver = 0;
                        *msg >> receiver;
                        Unicast(msg, receiver, client, receiver);
                        Unicast(msg, client, client, receiver);
                        break;
                    }
                    case MessageType::AUTH:
                    {
                        if (msg->size())
                        {
                            std::string key = msg->GetString();
                            for (auto itr : registeredNicknames)
                            {
                                if (itr.second == key)
                                    connections[client]->name = itr.first;
                            }
                        }

                        Message* msg = new Message(MessageType::AUTH, 4);
                        *msg << client;
                        msg->Send(client);
                        // send info about other clients
                        connectionMtx.lock();
                        for (auto itr : connections)
                        {
                            if (itr.first != client)
                            {
                                std::string& name = itr.second->name;
                                Message* msg = new Message(MessageType::SET_NICKNAME, name.length() + 4 + 1);
                                *msg << itr.first;
                                *msg << name.c_str();
                                msg->Send(client);
                            }
                        }
                        connectionMtx.unlock();

                        // broadcast new connection
                        {
                            std::string name = connections[client]->name;
                            Message* msg = new Message(MessageType::SET_NICKNAME, name.length() + 4 + 1);
                            *msg << client;
                            *msg << name.c_str();
                            Broadcast(msg);
                        }
                        break;
                    }
                    case MessageType::AUTH_REGISTER_NICK:
                    {
                        if (!msg->size())
                            break;

                        std::string nick = msg->GetString();
                        if (!registeredNicknames[nick].empty()) // key found
                        {
                                Message* msg = new Message(MessageType::NAME_REFUSED, 4);
                                msg->Send(client);
                        }
                        else
                        {
                            std::string key = GenerateKey();
                            registeredNicknames[nick] = key;

                            // save to file
                            std::ofstream file;
                            file.open("nick.reg", std::ios_base::app);
                            std::string out = nick + "*" + key;
                            DEBUG_LOG("Register key: " << out << std::endl);
                            file.write(out.c_str(), out.size());
                            file.close();

                            Message* msg = new Message(MessageType::SET_AUTH_KEY, key.size());
                            *msg << key.c_str();
                            msg->Send(client);
                        }
                        break;
                    }
                    case MessageType::SET_NICKNAME:
                    {
                        //printf("%u\n",strlen(msg->GetString()));
                        std::string nick(msg->GetString());
                        if (std::isdigit(nick[0]))
                            break;

                        // check for registered nicknames
                        if (!registeredNicknames[nick].empty()) // key found
                        {
                                Message* msg = new Message(MessageType::NAME_REFUSED, 4);
                                msg->Send(client);
                        }

                        connectionMtx.lock();
                        bool rename = true;
                        for (auto itr : connections)
                        {
                            if (itr.second->name.c_str() == nick)
                            {
                                rename = false;
                                break;
                            }
                        }
                        if (rename)
                        {
                            auto itr = registeredNicknames.find(connections[client]->name);
                            if  (itr != registeredNicknames.end())
                            {
                                //@TODO: Release nickname from file too!
                                // itr->first = nick;
                            }
                            connections[client]->name = nick;
                            connectionMtx.unlock();
                            Broadcast(msg,0,client);
                        }
                        else
                        {
                            connectionMtx.unlock();
                            Message* msg = new Message(MessageType::NAME_REFUSED, 4);
                            msg->Send(client);
                        }
                        break;
                    }
                    case MessageType::NAME_QUERY:
                    {
                        SOCKET target = 0;
                        *msg >> target;
                        std::string name = connections[target]->name;
                        Message* msg = new Message(MessageType::SET_NICKNAME, name.length() + 4 + 1);
                        *msg << target;
                        *msg << name.c_str();
                        msg->Send(client);
                        break;
                    }
                    case MessageType::ROLL:
                    {
                        Message* msg = new Message(MessageType::ROLL, 4 + 1 + 8);
                        *msg << client;
                        *msg << char(u_rand(1, 100));
                        *msg << Timer::GetCurTime();
                        Broadcast(msg);
                        delete msg;
                        break;
                    }
                    case MessageType::TICTACTOE_CHALLENGE:
                    case MessageType::LABYRINTH_CHALLENGE:
                    {
                        SOCKET opponent = 0;
                        *msg >> opponent;
                        Unicast(msg, opponent, client, opponent);
                        Unicast(msg, client, client, opponent);
                        break;
                    }
                    case MessageType::TICTACTOE_START_GAME:
                    {
                        SOCKET opponent = 0;
                        *msg >> opponent;
                        Message* msg = new Message(MessageType::TICTACTOE_START_GAME, 1);
                        bool roll = u_rand(0, 1);
                        *msg << roll;
                        Unicast(msg, opponent);
                        delete msg;
                        msg = new Message(MessageType::TICTACTOE_START_GAME, 1);
                        *msg << !roll;
                        Unicast(msg, client);
                        delete msg;
                        break;
                    }
                    case MessageType::LABYRINTH_START_GAME:
                    {
                        SOCKET opponent = 0;
                        *msg >> opponent;
                        Unicast(msg, client);
                        Unicast(msg, opponent);
                        break;
                    }
                    case MessageType::TICTACTOE_MOVE:
                    {
                        SOCKET opponent = 0;
                        *msg >> opponent;
                        Unicast(msg, opponent, client);
                        Unicast(msg, client, client);
                        break;
                    }
                    case MessageType::LABYRINTH_GAME_SEND_ARRAY:
                    case MessageType::LABYRINTH_GAME_SEND_MOVE:
                    {
                        SOCKET opponent = 0;
                        *msg >> opponent;
                        Unicast(msg, opponent, client);
                        break;
                    }
                    case MessageType::HTTP_REQUEST:
                    {
                        /*std::string str = msg->GetString();
                        size_t pos = str.find(' ');
                        // std::cout << str << std::endl;
                        char* url = new char[pos];
                        str.copy(url, pos - 1, 1);
                        url[pos-1] = '\0';
                        std::string reply =
                        std::string("<html>\n"
                                    "  <head>\n"
                                    "    <title>troll</title>\n"
                                    "  </head>\n"
                                    "  \n"
                                    "  <body>\n")
                                   +"    " + url +
                                    "\n"
                                    "  </body>\n"
                                    "</html>\n";
                        std::cout << reply << std::endl;
                        send(client, reply.c_str(), reply.size(), 0);
                        connectionMtx.lock();
                        closesocket(client);
                        auto itr = connections.find(client);
                        if (itr != connections.end())
                            connections.erase(itr);
                        connectionMtx.unlock();*/
                        break;
                    }
                    default:
                        break;
                }
                messages.pop_front();
            }
        }
    }
}

void NetworkConnection::ListenToConnections()
{
    SOCKET newClient;
    struct sockaddr_storage their_addr;
    socklen_t addr_size = sizeof(their_addr);
    while (true)
    {
        newClient = accept(_socketfd, (struct sockaddr*)&their_addr, &addr_size);
        if (IsSocketError(newClient))
        {
            continue;
        }
        else
        {
            DEBUG_LOG("client connected" << std::endl);
            connectionMtx.lock();
            connections[newClient] = new Client(new std::thread(&NetworkConnection::HandleConnection, this, newClient), newClient);
            connectionMtx.unlock();
        }
    }
}

void NetworkConnection::DecodeMessage(Message* /*msg*/)
{
    //if (msg->GetId() < MessageType::MSGS_MAX);
    //SendMsg(msg);
}

void NetworkConnection::Unicast(Message* msg, SOCKET receiver, SOCKET sendId, SOCKET originReceiver)
{
    uint16 len;
    const char* buffer;
    if (sendId)
    {
        bool isChatMessage = msg->GetId() < MessageType::MSGS_MAX;
        Message* message = new Message(msg->GetId(), msg->size(true) + 4 + (originReceiver ? 4 : 0) + (isChatMessage ? 8 : 0));
        if (originReceiver)
            *message << originReceiver;
        *message << sendId;
        if (isChatMessage)
            *message << Timer::GetCurTime();
        message->Append(msg->GetString(), msg->size());
        buffer = message->ToPacket(len);
        delete message;
    }
    else
        buffer = msg->ToPacket(len);
    send(receiver, buffer, len, 0);
}

void NetworkConnection::Broadcast(Message* msg, SOCKET excluded, SOCKET sendId)
{
    uint16 len;
    const char* buffer;
    if (sendId)
    {
        bool isChatMessage = msg->GetId() < MessageType::MSGS_MAX;
        Message* message = new Message(msg->GetId(), msg->size() + 4 + (isChatMessage ? 8 : 0));
        *message << sendId;
        if (isChatMessage)
            *message << Timer::GetCurTime();
        message->Append(msg->GetString(), msg->size());
        buffer = message->ToPacket(len);
        delete message;
    }
    else
        buffer = msg->ToPacket(len);

    connectionMtx.lock();
    for (auto itr = connections.begin(); itr != connections.end(); ++itr)
    {
        if (itr->first == excluded)
            continue;
        send(itr->first, buffer, len, 0);
    }
    connectionMtx.unlock();
}

std::string NetworkConnection::GenerateKey()
{
    srand(time(NULL));
    std::ostringstream key;
    key << std::hex << rand() << rand() << rand() << rand() << rand() << rand() << rand() << rand() << std::endl;
    return key.str();
}
