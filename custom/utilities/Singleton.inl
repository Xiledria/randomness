#ifndef __SINGLETON_H
#define __SINGLETON_H
#include <cstdlib>
template<typename T>
class Singleton
{
public :
    static T* Instance();
    static void Destroy();

protected :
    inline explicit Singleton()
    {
        assert(Singleton::instance_ == NULL);
        Singleton::instance_ = static_cast<T*>(this);
    }
    inline ~Singleton() {
        Singleton::instance_ = NULL;
    }

private :
    static T* CreateInstance();
    static void ScheduleForDestruction(void (*)());
    static void DestroyInstance(T*);

private :
    static T* instance_;

private :
    inline explicit Singleton(Singleton const&) {}
    inline Singleton& operator=(Singleton const&) { return *this; }
};


template<typename T>
T* Singleton<T>::Instance()
{
    if ( Singleton::instance_ == NULL )
    {
        Singleton::instance_ = CreateInstance();
        ScheduleForDestruction(Singleton::Destroy);
    }
    return (Singleton::instance_);
}

template<typename T>
void Singleton<T>::Destroy()
{
    if ( Singleton::instance_ != NULL )
    {
        DestroyInstance(Singleton::instance_);
        Singleton::instance_ = NULL;
    }
}

template<typename T>
inline T* Singleton<T>::CreateInstance()
{
    return new T();
}

template<typename T>
inline void Singleton<T>::ScheduleForDestruction(void (*pFun)())
{
    std::atexit(pFun);
}

template<typename T>
inline void Singleton<T>::DestroyInstance(T* p)
{
    delete p;
}

template<typename T>
T* Singleton<T>::instance_ = NULL;

#endif
