#ifndef __FUNCTIONS_H
#define __FUNCTIONS_H

#include "SharedDefs.h"


bool u_roll(uint32 chance);
//inPerc = true - input is in percentages (example - 85,3)
//inPerc = false - input is deficit (example 0,853)
bool f_roll(float chance, bool inPerc);
int32 i_rand(int32 min, int32 max);
uint32 u_rand(uint32 min, uint32 max);
float f_rand(float min, float max);

#endif