#ifndef __ASSERT_H
#define __ASSERT_H
#ifdef NDEBUG
#undef NDEBUG
#define TMP_NDEBUG
#endif
#include <cassert>
#ifdef TMP_NDEBUG
#define NDEBUG 1
#endif
#endif