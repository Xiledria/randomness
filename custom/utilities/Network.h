#ifndef __NETWORK_H
#define __NETWORK_H

#include "SharedDefs.h"
#include "Message.h"
#include <thread>
#include <map>
#include <deque>
#include <mutex>
#if defined(__linux__)
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#define IsSocketError(socket) (socket < 0)
#else
#define IsSocketError(socket) (socket == 0)
#include <winsock2.h>
#include <ws2tcpip.h>
#endif
#define SERVER_PORT "15935"

class Client
{
public:
    Client(std::thread* thrd,SOCKET name){this->thrd = thrd; this->name = std::to_string(name);}
    ~Client(){ name.clear(); delete thrd;}
    std::string  name;
    std::thread* thrd;
};

class NetworkConnection
{
public:
    static void GetMessages(std::deque<Message*>& messages, ssize_t rcvbytes, char* rcvbuffer);
public:
    NetworkConnection();
    ~NetworkConnection();
    void ListenToConnections();
    void HandleConnection(SOCKET client);
    void Unicast(Message* msg, SOCKET receiver, SOCKET sendId = 0, SOCKET originReceiver = 0);
    void Broadcast(Message* msg, SOCKET excluded = 0, SOCKET sendId = 0);
    void DecodeMessage(Message* msg);
    void SendMsg(Message* msg);
    std::string GenerateKey();
protected:
    void Initialize();
    addrinfo* _info;
    std::thread* _thread;
    std::mutex connectionMtx;
    SOCKET _socketfd; // server socket which handles all connections
    std::map<SOCKET,Client*> connections; //client sockets with handling threads
    std::map<std::string/*name*/, std::string/*key*/> registeredNicknames;
};

#endif
