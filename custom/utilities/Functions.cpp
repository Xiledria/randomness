#include "Functions.h"
#include <cstdlib>


bool u_roll(uint32 chance)
{
    return chance < uint32(rand()%101);
}

//inPerc = true - input is in percentages (example - 85,3)
//inPerc = false - input is deficit (example 0,853)
bool f_roll(float chance, bool inPerc)
{
    chance *= inPerc ? 100 : 10000;
    float roll = float(rand()%10001);
    return chance > roll;
}

int32 i_rand(int32 min, int32 max)
{
    int32 result = rand()%max; return result > min ? result : result + min;
}

uint32 u_rand(uint32 min, uint32 max)
{
    uint32 result = rand()%max; return result > min ? result : result + min; 
}

float f_rand(float min, float max)
{
    min *= 1000; max *= 1000; float result = float(rand()%(int32)max); return result > min ? result / 1000 : (result + min) / 1000; 
}