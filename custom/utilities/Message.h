#ifndef __MESSAGE_H
#define __MESSAGE_H

#include "SharedDefs.h"
#include <cstring>
#include "cssrt.h"
#ifdef _WIN32
#include <winsock2.h>
#endif
enum class MessageType : int16
{
    MESSAGES_START,
    MSG_BROADCAST = MESSAGES_START,
    MSG_WHISPER,
    MSGS_MAX,
    AUTH = MSGS_MAX,
    AUTH_REGISTER_NICK,
    SET_AUTH_KEY,
    SET_NICKNAME,
    NAME_QUERY,
    NAME_REFUSED,
    CLIENT_DISCONNECTED,
    ROLL,
    TICTACTOE_CHALLENGE, // challenge msg types must be outside of GAMES scope, otherwise it won't be handled because of non-existing game
    TICTACTOE_START_GAME,
    LABYRINTH_CHALLENGE,
    LABYRINTH_START_GAME,
    GAMES_START,
    TICTACTOE_START = GAMES_START,
    TICTACTOE_MOVE = TICTACTOE_START,
    TICTACTOE_END,
    TROLL_GAME_START = TICTACTOE_END,
    TROLL_GAME_END,
    LABYRINTH_GAME_START = TROLL_GAME_END,
    LABYRINTH_GAME_SEND_ARRAY,
    LABYRINTH_GAME_SEND_MOVE,
    LABYRINTH_GAME_END,
    GAMES_END = LABYRINTH_GAME_END,
    MESSAGES_END = GAMES_END,
    // special cases
    HTTP_REQUEST = 8276,
};

class Message
{
public:
    static uint8 GetBufferByteSize();
public:
    Message(char* data, uint16 rcvbytes);
    /*SIZE NEEDS TO BE SET >= OF REAL SIZE, OTHERWISE IT MIGHT CAUSE PROBLEMS*/
    Message(MessageType id, uint16 size);
    ~Message();

    char* ToPacket(uint16& length);
    uint16 size(bool whole = false);
    char* GetString(uint16 len = 0);
    void Send(SOCKET target);
    template<typename T>
    void operator>>(T& value)
    {
        if (_size < begin+sizeof(T))
            return;
        memcpy(&value,buffer+begin,sizeof(T));
        begin += sizeof(T);
    }
    //char array is handled differently than
    //other params that are pushed into buffer
    void Append(const char* value);
    void Append(const char* value, uint16 len);
    void operator<<(std::string value);
    void operator<<(const char* value);
    void operator<<(char* value);
    template<typename T>
    void operator<<(T value)
    {
        memcpy(buffer+end,&value,sizeof(T));
        end += sizeof(T);
    }
    MessageType GetId();
protected:
    MessageType id;
    uint16 _size;
    uint16 begin;
    uint16 end;
    char* buffer;
};

#endif
