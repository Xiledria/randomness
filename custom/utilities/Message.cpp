#include "Message.h"
#include "Network.h"
#include <algorithm>
#include <iostream>

Message::Message(char* data, uint16 rcvbytes)
{
    this->id = MessageType(0);
    memcpy((uint8*)&this->id, data, sizeof(this->id));
    buffer = new char[rcvbytes - sizeof(this->id) + 1];
    memcpy(buffer, data + sizeof(this->id), rcvbytes - sizeof(this->id));
    this->_size = rcvbytes - sizeof(this->id);
    begin = 0;
    end = 0;
}

Message::Message(MessageType id, uint16 size)
{
    this->id = id;
    this->_size = size;
    buffer = new char[size+1];
    buffer[size] = 0;
    end = 0;
    begin = 0;
}

Message::~Message()
{
    delete[] buffer;
}

char* Message::GetString(uint16 len)
{
    char* tmp = buffer+begin;
    if (size() == 0)
        return nullptr;

    if (len)
    {
        std::string tmpstr = tmp;
        tmpstr.resize(len);
        begin += len;
        char* tmp = new char[tmpstr.size() + 1];
        memcpy(tmp, tmpstr.c_str(), tmpstr.size());
        tmp[tmpstr.size()] = 0;
        return tmp;
    }
    else
        tmp[size()] = 0;
    return tmp;
}

void Message::Append(const char *value)
{
    uint32 len = uint32(strlen(value));
    memcpy(buffer+end,value,len);
    end += len;
}

void Message::Append(const char *value, uint16 len)
{
    memcpy(buffer+end,value,len);
    end += len;
}

void Message::operator<<(std::string value)
{
    *this << value.c_str();
}

void Message::operator<<(char* value)
{
    *this << (const char*)value;
}

void Message::operator<<(const char* value)
{
    uint32 len = uint32(strlen(value)+1);
    memcpy(buffer+end,value,len);
    end += len;
}

uint16 Message::size(bool whole)
{
    return whole  ? _size : std::max(int32(_size-begin),int32(0));
}
char* Message::ToPacket(uint16& length)
{
    length = _size + sizeof(this->id) + GetBufferByteSize();
    char* packet = new char[length + 1];
    uint16 size = _size + sizeof(this->id);
    memcpy(packet, &size, sizeof(size));
    memcpy(packet + GetBufferByteSize(), &this->id, sizeof(this->id));
    memcpy(packet + sizeof(this->id) + GetBufferByteSize(), buffer, _size);
    packet[length] = 0;
    return packet;
}

void Message::Send(SOCKET target)
{
    uint16 len;
    const char* buffer = ToPacket(len);
    send(target, buffer, len, 0);
    delete buffer;
    delete this;
}

MessageType Message::GetId()
{
    return id;
}

uint8 Message::GetBufferByteSize()
{
    return sizeof(_size);
}
