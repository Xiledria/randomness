#ifndef __TIMER_H
#define __TIMER_H

#include "SharedDefs.h"
#include <mutex>          // std::mutex
#ifdef _WIN32
#undef NOMINMAX
#define NOMINMAX //remove stupid Windows defines
#include <windows.h>
#define wait(x) Sleep(x) /*milliseconds*/
#else
#include <sys/time.h>
#include <unistd.h>
#define wait(x) usleep(1000*x) /*milliseconds*/
#endif


class Timer // ms timer
{
public:
    // ms timer
    Timer(uint64 interval) : _startTime(GetCurTime())
    {
        _interval = interval;
        _diff = 0;
    }

    ~Timer()
    {
    }
#if defined(__linux__) || defined(__CYGWIN__)
    static uint64 GetCurTime()
    {
        timeval te; 
        gettimeofday(&te, NULL); // get current time
        uint64 milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
        return milliseconds;
    }
#elif _WIN32 
    static uint64 GetCurTime()
    {
        return GetTickCount64();
    }
#endif
    bool elapsed()
    {
        _mtx.lock();
        uint64 timer = GetCurTime();
        if (timer >= _startTime + _interval)
        {
            _diff = timer - _startTime;
            _startTime += _diff;
            _mtx.unlock();
            return true;
        }
        _mtx.unlock();
        return false;
    }

private:
    uint64 _interval;
    uint64 _startTime;
    uint64 _diff;
    std::mutex _mtx;
};

#endif
