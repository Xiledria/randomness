#ifndef __SHAREDDEFS_H
#define __SHAREDDEFS_H

#include <cstdint>
#include <string>

// #define DEBUG_OUTPUT

#ifdef DEBUG_OUTPUT
#define DEBUG_LOG(x) std::cout << x
#else
#define DEBUG_LOG(x)
#endif
typedef   int8_t          int8;
typedef   int16_t        int16;
typedef   int32_t        int32;
typedef   int64_t        int64;
typedef   uint8_t        uint8;
typedef   uint16_t      uint16;
typedef   uint32_t      uint32;
typedef   uint64_t      uint64;

#if defined(__linux__)
#define closesocket close
#define SOCKET int32
#elif defined(WIN32)
#include <winsock2.h>
#endif

#if __ANDROID__
#include <sstream>

namespace std
{
    template <typename T>
    std::string to_string(T value)
    {
        std::ostringstream os;
        os << value;
        return os.str();
    }
}
#endif

#include "Functions.h" // needs to be below typedefs


enum Happiness
{
    HAPPINESS_LEAST = -100,
    HAPPINESS_MAX   =  100
};

enum Relation
{
    RELATION_WORST      = -100,
    RELATION_HATE       =  -90,
    RELATION_UNFRIENDLY =  -50,
    RELATION_NEUTRAL    =    0,
    RELATION_FRIENDLY   =   50,
    RELATION_LOVE       =   90,
    RELATION_BEST       =  100
};

enum TypeId
{
    TYPEID_SOLAMAYA,
    TYPEID_ARLANDIA,
    TYPEID_HUMAN,
};

enum Gender
{
    GENDER_FEMALE,
    GENDER_MALE
};

enum DeathState
{
    DEATHSTATE_DEAD,
    DEATHSTATE_ALIVE
};

struct Position
{
    float x;
    float y;
    float z;
};

#endif
