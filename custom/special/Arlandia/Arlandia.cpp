#include "Arlandia.h"
#include "Something.h"

Arlandia::Arlandia() : Human(GENDER_FEMALE,"Arlandia",false)
{
    typeId = TYPEID_ARLANDIA;
    happinessTimer = new Timer(600000);
    _thrd = new std::thread(&Arlandia::KeepAlive,this);
}

void Arlandia::KeepAlive()
{
    for(;;)
    {
        wait(50);
        _thrdMsgMtx.lock();
        if (!_messages.empty())
            HandleMessage(*_messages.begin());
        _thrdMsgMtx.unlock();
        if (happinessTimer->elapsed())
            SetHappiness(GetHappiness()-1);
    }
}
