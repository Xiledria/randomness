#ifndef __ARLANDIA_H
#define __ARLANDIA_H

#include "Human.h"
#include "Timer.h"


class Arlandia : public Human
{
public:
    Arlandia();
    ~Arlandia(){}
protected:
    void KeepAlive();
    Timer* happinessTimer;
};

#endif
